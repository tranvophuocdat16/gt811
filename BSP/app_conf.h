#ifndef __APP_CONF_H_
#define __APP_CONF_H_

#ifdef __cplusplus
extern "C" {
#endif

// types
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <ctype.h>
#include <stdlib.h>

	
#include "main.h"
// #include "cmsis_os.h"
#define __IO volatile
	
// HieuNT: add for convenient
#define _sizeof(x) (sizeof(x)-1)
// #define _malloc(x) pvPortMalloc(x)
// #define _free(x) vPortFree(x)
#define delay(x) HAL_Delay(x)

#define millis() HAL_GetTick()
// void DWT_Delay(uint32_t us);
// #define delay_us(us) DWT_Delay(us)
// void delay_us(uint32_t us);
// uint32_t micros(void);
// #define delayMicroseconds(us) delay_us(us)

#define __SET_BIT(reg, x) do {reg |= x;} while(0)
#define __CLR_BIT(reg, x) do {reg &= ~(x);} while(0)	
#define TRUE true
#define FALSE false

#ifdef __cplusplus
}
#endif

#endif
