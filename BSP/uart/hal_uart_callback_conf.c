/*
  HAL UART CALLBACK HANDLER CONFIG
  
  HieuNT
  
  21/7/2016
*/

#include "uart_services.h"

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart == &huart1){
		UART_DMA_TX_DMAComplete(&huart1_dma);
	}
	else if (huart == &huart2){
		UART_DMA_TX_DMAComplete(&huart2_dma);
	}
	else if (huart == &huart3){
		UART_DMA_TX_DMAComplete(&huart3_dma);
	}
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart == &huart1){
		UART_DMA_RX_DMAComplete(&huart1_dma);
	}
	else if (huart == &huart2){
		UART_DMA_RX_DMAComplete(&huart2_dma);
	}
	else if (huart == &huart3){
		UART_DMA_RX_DMAComplete(&huart3_dma);
	}
}

void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
	if (huart == &huart1){
		UART_DMA_RX_StartDMA(&huart1_dma);
	}
	else if (huart == &huart2){
		UART_DMA_RX_StartDMA(&huart2_dma);
	}
	else if (huart == &huart3){
		UART_DMA_RX_StartDMA(&huart3_dma);
	}
}
