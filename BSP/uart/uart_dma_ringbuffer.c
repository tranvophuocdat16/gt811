/**
	Source file for UART DMA using as ringbuffer implementation!
	
	HieuNT
	17/7/2015
*/


// TODO: remove hdma_uart_rx & hdma_uart_tx in UART_DMA_RINGBUFFER_T
// TODO: since dma already linked in huart

#include "app_conf.h"
#include "uart_dma_ringbuffer.h"


__weak void UART_DMA_Delay(uint32_t ms)
{
	HAL_Delay(ms);
}

void UART_DMA_Init(UART_DMA_RINGBUFFER_T *huart_dma, 
												UART_HandleTypeDef *huart,
												DMA_HandleTypeDef *hdma_uart_rx,
												DMA_HandleTypeDef *hdma_uart_tx, 
												uint8_t *rxBuffer, uint8_t *txBuffer,
												uint32_t rxBufferSize, uint32_t txBufferSize,
												void (*rxInterruptCallback)(void *huart_dma))
{
	huart_dma->huart = huart;
	huart_dma->hdma_uart_rx = hdma_uart_rx;
	huart_dma->hdma_uart_tx = hdma_uart_tx;
	huart_dma->rxBuffer = rxBuffer;
	huart_dma->txBuffer = txBuffer;
	huart_dma->txBufferSize = txBufferSize;
	huart_dma->rxBufferSize = rxBufferSize;
	RINGBUF_Init(&huart_dma->txRingbuffer, huart_dma->txBuffer, huart_dma->txBufferSize);
	RINGBUF_Init(&huart_dma->rxRingbuffer, huart_dma->rxBuffer, huart_dma->rxBufferSize);
	huart_dma->rxInterruptCallback = rxInterruptCallback;
	huart_dma->currentDMATxSize = 0;
	huart_dma->isSending = 0;
	//huart_dma->rxDmaState = UART_DMA_RX_STATE_STOPPED;
}

void UART_DMA_TX_SendByte(UART_DMA_RINGBUFFER_T *huart_dma, uint8_t c)
{
	while (!RINGBUF_Put(&huart_dma->txRingbuffer, c)){ // to make sure no character missing
		UART_DMA_Delay(5);
	}
	UART_DMA_TX_StartDMA(huart_dma);
}

void UART_DMA_TX_SendString(UART_DMA_RINGBUFFER_T *huart_dma, uint8_t *s)
{
	while(*s){
		while (!RINGBUF_Put(&huart_dma->txRingbuffer, *s)){ // to make sure no character missing
			// dbg("tx overflow\n\r");
			UART_DMA_TX_StartDMA(huart_dma);
			UART_DMA_Delay(10);
		}
		s++;
	}
	UART_DMA_TX_StartDMA(huart_dma);
}

void UART_DMA_TX_SendBlock(UART_DMA_RINGBUFFER_T *huart_dma, uint8_t *buffer, uint32_t size)
{
	__IO uint32_t remain = size, free, i;
	uint8_t *buf = buffer;
	
	while (remain){
		free = RINGBUF_Free(&huart_dma->txRingbuffer);
		if (free == 0) {
			UART_DMA_Delay(10);
			continue; // to make sure no data missing
		}
		if (free >= remain){
			for (i=0;i<remain;i++){
				RINGBUF_Put(&huart_dma->txRingbuffer, *buf++);
			}
			UART_DMA_TX_StartDMA(huart_dma);
			remain = 0;
		}
		else {
			for (i=0;i<free;i++){
				RINGBUF_Put(&huart_dma->txRingbuffer, *buf++);
			}
			UART_DMA_TX_StartDMA(huart_dma);
			remain -= free;
		}
	}
}

// This must be called in HAL_UART_TxCpltCallback()
void UART_DMA_TX_DMAComplete(UART_DMA_RINGBUFFER_T *huart_dma)
{
	RINGBUF_UpdateTail(&huart_dma->txRingbuffer, huart_dma->currentDMATxSize);
	huart_dma->isSending = 0;
	if (RINGBUF_Available(&huart_dma->txRingbuffer)){
		UART_DMA_TX_StartDMA(huart_dma);
	}
}

// Private
void UART_DMA_TX_StartDMA(UART_DMA_RINGBUFFER_T *huart_dma)
{
	int32_t size;
	uint8_t *p;
	if (huart_dma->isSending) return;
	
	if (RINGBUF_Available(&huart_dma->txRingbuffer)){
		size = RINGBUF_CountInSegment(&huart_dma->txRingbuffer);
		huart_dma->currentDMATxSize = size;
		p = RINGBUF_GetTailPointer(&huart_dma->txRingbuffer);
		huart_dma->isSending = 1;
		HAL_UART_Transmit_DMA(huart_dma->huart, p, size);
	}
}

// This should be call before any actual rx (one time)
void UART_DMA_RX_StartDMA(UART_DMA_RINGBUFFER_T *huart_dma)
{
	//huart_dma->rxDmaState = UART_DMA_RX_STATE_RUNNING;
	__HAL_UART_CLEAR_OREFLAG(huart_dma->huart);
	HAL_UART_Receive_DMA(huart_dma->huart, huart_dma->rxBuffer, huart_dma->rxBufferSize);
	//__HAL_UART_ENABLE_IT(huart_dma->huart, UART_IT_RXNE);
}

/* // dma v2
// Private
void UART_DMA_RX_PauseDMA(UART_DMA_RINGBUFFER_T *huart_dma)
{
	huart_dma->rxDmaState = UART_DMA_RX_STATE_PAUSED;
	CLEAR_BIT(huart_dma->huart->Instance->CR3, USART_CR3_DMAR);
}

// Private
void UART_DMA_RX_ResumeDMA(UART_DMA_RINGBUFFER_T *huart_dma)
{
	huart_dma->rxDmaState = UART_DMA_RX_STATE_RUNNING;
	__HAL_UART_CLEAR_OREFLAG(huart_dma->huart);
  SET_BIT(huart_dma->huart->Instance->CR3, USART_CR3_DMAR);
}
*/

// This must be called in HAL_UART_RxCpltCallback()
void UART_DMA_RX_DMAComplete(UART_DMA_RINGBUFFER_T *huart_dma)
{
	// Restart DMA for next receive!
	//if (huart_dma->rxDmaState == UART_DMA_RX_STATE_RUNNING){
		// dma v2: always enable for next receive
		UART_DMA_RX_StartDMA(huart_dma);
	//}
}

uint32_t UART_DMA_RX_Available(UART_DMA_RINGBUFFER_T *huart_dma)
{
	// dma v2, don't care overflow issue!
	RINGBUF_SetHead(&huart_dma->rxRingbuffer, huart_dma->rxBufferSize - __HAL_DMA_GET_COUNTER(huart_dma->hdma_uart_rx));
	return (uint32_t)RINGBUF_Available(&huart_dma->rxRingbuffer);
}

uint32_t UART_DMA_RX_Count(UART_DMA_RINGBUFFER_T *huart_dma)
{
	// dma v2, don't care overflow issue!
	RINGBUF_SetHead(&huart_dma->rxRingbuffer, huart_dma->rxBufferSize - __HAL_DMA_GET_COUNTER(huart_dma->hdma_uart_rx));
	return (uint32_t)RINGBUF_Count(&huart_dma->rxRingbuffer);
}

// return: 0 if no data availalbe
int32_t UART_DMA_RX_Get(UART_DMA_RINGBUFFER_T *huart_dma, uint8_t *c)
{
	int32_t res = RINGBUF_Get(&huart_dma->rxRingbuffer, c);
	/* // dma v2
	if (huart_dma->rxDmaState == UART_DMA_RX_STATE_PAUSED){
		//dbg("resume rx dma\n\r");
		UART_DMA_RX_ResumeDMA(huart_dma);
	}
	else if (huart_dma->rxDmaState == UART_DMA_RX_STATE_STOPPED){
		//dbg("start rx dma\n\r");
		UART_DMA_RX_StartDMA(huart_dma);
	}
	*/
	return res;
}

// Return: size of block has been read (non-blocking function)
uint32_t UART_DMA_RX_GetBlock(UART_DMA_RINGBUFFER_T *huart_dma, uint8_t *buffer, uint32_t size)
{
	__IO uint32_t remain = size, available, count = 0, i;
	uint8_t *buf = buffer;
	
	while(remain){
		// Bug fixed for dma v2
		// available = RINGBUF_Count(&huart_dma->rxRingbuffer);
		available = UART_DMA_RX_Count(huart_dma);
		if (available == 0) return count;
		if (remain <= available){
			for (i=0;i<remain;i++){
				RINGBUF_Get(&huart_dma->rxRingbuffer, buf++);
			}
			count += remain;
			remain = 0;
		}
		else {
			for (i=0;i<available;i++){
				RINGBUF_Get(&huart_dma->rxRingbuffer, buf++);
			}
			count += available;
			remain -= available;
		}

		#if 0 // dma v2
		if (huart_dma->rxDmaState == UART_DMA_RX_STATE_PAUSED){
			//dbg("resume rx dma\n\r");
			UART_DMA_RX_ResumeDMA(huart_dma);
		}
		else if (huart_dma->rxDmaState == UART_DMA_RX_STATE_STOPPED){
			//dbg("start rx dma\n\r");
			UART_DMA_RX_StartDMA(huart_dma);
		}
		#endif
	}
	return count;
}

void UART_DMA_RX_Flush(UART_DMA_RINGBUFFER_T *huart_dma)
{
	RINGBUF_Flush(&huart_dma->rxRingbuffer);
}

/* // dma v2: don't care overflow issu
// This should process earlier than (before) HAL_UART_IRQHandler()
void UART_DMA_IRQHandler(UART_DMA_RINGBUFFER_T *huart_dma)
{
	uint32_t free;
	uint32_t tmp_flag = __HAL_UART_GET_FLAG(huart_dma->huart, UART_FLAG_RXNE);
  uint32_t tmp_it_source = __HAL_UART_GET_IT_SOURCE(huart_dma->huart, UART_IT_RXNE);
	
 // USER CODE END USART1_IRQn 0
	if ((tmp_it_source != RESET)){
		if (huart_dma->rxDmaState == UART_DMA_RX_STATE_RUNNING){
			RINGBUF_SetHead(&huart_dma->rxRingbuffer, huart_dma->rxBufferSize - huart_dma->hdma_uart_rx->Instance->CNDTR);
			free = RINGBUF_Free(&huart_dma->rxRingbuffer);
			//dbg("head %u tail %u free %u\n\r", huart_dma->rxRingbuffer.head, huart_dma->rxRingbuffer.tail, free);
			if (free == 0){
				//dbg("rx ovf\n\r");
				if (huart_dma->rxRingbuffer.head != 0){
					//dbg("pause dma\n\r");
					UART_DMA_RX_PauseDMA(huart_dma);
				}
				else {
					// if free == 0 and head == 0 (wrap-around) then dma will be stopped
					// automatically (and callback), don't need pause dma!
					huart_dma->rxDmaState = UART_DMA_RX_STATE_STOPPED;
				}
			}
			if (huart_dma->rxInterruptCallback != NULL) huart_dma->rxInterruptCallback((void *)huart_dma);
		}
		else { // DMA stopped or paused ==> Rx interrupt: not empty in Rx --> just empty the RX register to clear flag!
			if (tmp_flag != RESET) tmp_flag = huart_dma->huart->Instance->DR;
		}
	}
}
*/
