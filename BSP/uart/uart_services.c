/**
	Source file for uart services
	
	HieuNT
	23/7/2015
*/

#include "uart_services.h"

uint8_t uart1_tx_buffer[UART1_TX_BUFFER_SIZE];
uint8_t uart1_rx_buffer[UART1_RX_BUFFER_SIZE];
uint8_t uart2_tx_buffer[UART2_TX_BUFFER_SIZE];
uint8_t uart2_rx_buffer[UART2_RX_BUFFER_SIZE];

uint8_t uart3_tx_buffer[UART3_TX_BUFFER_SIZE];
uint8_t uart3_rx_buffer[UART3_RX_BUFFER_SIZE];

UART_DMA_RINGBUFFER_T huart1_dma;
UART_DMA_RINGBUFFER_T huart2_dma;
UART_DMA_RINGBUFFER_T huart3_dma;

void UART_SERVICES_Init(void)
{
	UART_DMA_Init(&huart1_dma, &huart1, &hdma_usart1_rx, &hdma_usart1_tx, 
		uart1_rx_buffer, uart1_tx_buffer, UART1_RX_BUFFER_SIZE, UART1_TX_BUFFER_SIZE, NULL);
	
	UART_DMA_Init(&huart2_dma, &huart2, &hdma_usart2_rx, &hdma_usart2_tx, 
		uart2_rx_buffer, uart2_tx_buffer, UART2_RX_BUFFER_SIZE, UART2_TX_BUFFER_SIZE, NULL);
  
	UART_DMA_Init(&huart3_dma, &huart3, &hdma_usart3_rx, &hdma_usart3_tx, 
		uart3_rx_buffer, uart3_tx_buffer, UART3_RX_BUFFER_SIZE, UART3_TX_BUFFER_SIZE, NULL);

    UART_DMA_RX_StartDMA(&huart1_dma);
	UART_DMA_RX_StartDMA(&huart2_dma);
	UART_DMA_RX_StartDMA(&huart3_dma);
}
