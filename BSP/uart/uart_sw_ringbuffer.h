/**
	Header file for UART SW using ringbuffer implementation!
	
	HieuNT
	17/7/2015
*/


#ifndef __UART_SW_RINGBUFFER__
#define __UART_SW_RINGBUFFER__

#ifdef __cplusplus
 extern "C" {
#endif

#include "ringbuf_dma.h"

typedef struct {
	UART_HandleTypeDef *huart;
	uint8_t *rxBuffer;
	uint8_t *txBuffer;
	uint8_t rxByte; // private
	uint8_t txByte; // private
	uint32_t rxBufferSize;
	uint32_t txBufferSize;
	RINGBUF rxRingbuffer;
	RINGBUF txRingbuffer;
	__IO int32_t isSending; // should be atomic
	__IO int32_t isReceiving; // should be atomic
	void (*rxInterruptCallback)(void *huart_sw_ringbuffer); // can be used for semaphore synchronization
} UART_SW_RINGBUFFER_T;

void UART_SW_Init(UART_SW_RINGBUFFER_T *huart_sw, 
												UART_HandleTypeDef *huart,
												uint8_t *rxBuffer, uint8_t *txBuffer,
												uint32_t rxBufferSize, uint32_t txBufferSize,
												void (*rxInterruptCallback)(void *huart_sw));
void UART_SW_TX_SendByte(UART_SW_RINGBUFFER_T *huart_sw, uint8_t c);
void UART_SW_TX_SendString(UART_SW_RINGBUFFER_T *huart_sw, uint8_t *s);
void UART_SW_TX_SendBlock(UART_SW_RINGBUFFER_T *huart_sw, uint8_t *buffer, uint32_t size);
void UART_SW_TX_Complete(UART_SW_RINGBUFFER_T *huart_sw);
void UART_SW_TX_Start(UART_SW_RINGBUFFER_T *huart_sw);
void UART_SW_RX_Start(UART_SW_RINGBUFFER_T *huart_sw);
void UART_SW_RX_Complete(UART_SW_RINGBUFFER_T *huart_sw);
uint32_t UART_SW_RX_Available(UART_SW_RINGBUFFER_T *huart_sw);
uint32_t UART_SW_RX_Count(UART_SW_RINGBUFFER_T *huart_sw);
int32_t UART_SW_RX_Get(UART_SW_RINGBUFFER_T *huart_sw, uint8_t *c);
uint32_t UART_SW_RX_GetBlock(UART_SW_RINGBUFFER_T *huart_sw, uint8_t *buffer, uint32_t size);
void UART_SW_RX_Flush(UART_SW_RINGBUFFER_T *huart_sw);

// Link in specific env / or using HAL_Delay if not implemented
void UART_SW_Delay(uint32_t ms);

#ifdef __cplusplus
 }
#endif

#endif // __UART_DMA_RINGBUFFER__
