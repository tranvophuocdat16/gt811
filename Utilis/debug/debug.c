#include "debug.h"
#include <stdarg.h>
#include <stdio.h>

#include "debug_uart_conf.h"
#include "uart/uart_services.h"
#define DEBUG_BUFF_MAX_SIZE 512
//#define DEBUG_USE_RTOS
#ifdef DEBUG_USE_RTOS
#include "cmsis_os.h"
#include "FreeRTOS.h"
#include "semphr.h"
SemaphoreHandle_t dbg_semHandler = NULL;
#endif

static uint8_t buffer[DEBUG_BUFF_MAX_SIZE];
void user_debug_init(void)
{
	#ifdef DEBUG_USE_RTOS
	dbg_semHandler = xSemaphoreCreateMutex();
	#endif
}

static void uart_printf(const char *fmt, ...)
{
	uint32_t len;
	va_list vArgs;
	va_start(vArgs, fmt);
	len = vsprintf((char *)&buffer[0], (char const *)fmt, vArgs);
	va_end(vArgs);
	UART_DMA_TX_SendBlock (UART_DEBUG, buffer, len);
}
static void uart_vprintf(const char *fmt, va_list vArgs)
{
	uint32_t len;
	len = vsprintf((char *)&buffer[0], (char const *)fmt, vArgs);
	UART_DMA_TX_SendBlock (UART_DEBUG, buffer, len);
}
#define __debug_printf(fmt, ...) uart_printf(fmt, __VA_ARGS__)
#define __debug_vprintf(fmt, vArgs) uart_vprintf(fmt, vArgs)
void user_debug_print(int level, const char* module, int line, const char* fmt, ...)
{
	#ifdef DEBUG_USE_RTOS
	xSemaphoreTake( dbg_semHandler, ( TickType_t ) osWaitForever );
	#endif
	switch (level){
		case 1:
			// "->[ERROR](module:line): "
			__debug_printf("->[ERROR](%s:%d): ", module, line);
			break;
		case 2:
			// "->[WARN](module:line): "
			__debug_printf("->[WARN](%s:%d): ", module, line);
			break;
		case 3:
			// "->[INFO](module:line): "
			__debug_printf("->[INFO](%s:%d): ", module, line);
			break;
		case 4:
		default:
			// Don't need to print DEBUG :P
			__debug_printf("->(%s:%d): ", module, line);
			break;
	}

	va_list     vArgs;		    
	va_start(vArgs, fmt);
	__debug_vprintf((char const *)fmt, vArgs);
	va_end(vArgs);

	#ifdef DEBUG_USE_RTOS
	// osSemaphoreRelease(dbgSem_id);
	xSemaphoreGive( dbg_semHandler );
	#endif
}

void dbg_error(const char* module, int line, int ret)
{
	#ifdef DEBUG_USE_RTOS
	xSemaphoreTake( dbg_semHandler, ( TickType_t ) osWaitForever );
	#endif

	__debug_printf("[RC] Module %s - Line %d : Error %d\n", module, line, ret);
	
	#ifdef DEBUG_USE_RTOS
	xSemaphoreGive( dbg_semHandler );
	#endif
}

void user_debug_print_exact(const char* fmt, ...)
{
	#ifdef DEBUG_USE_RTOS
	xSemaphoreTake( dbg_semHandler, ( TickType_t ) osWaitForever );
	#endif

	va_list     vArgs;		    
	va_start(vArgs, fmt);
	__debug_vprintf((char const *)fmt, vArgs);
	va_end(vArgs);
	
	#ifdef DEBUG_USE_RTOS
	xSemaphoreGive( dbg_semHandler );
	#endif
}

void user_debug_native(char *string, int len)
{
	#ifdef DEBUG_USE_RTOS
	xSemaphoreTake( dbg_semHandler, ( TickType_t ) osWaitForever );
	#endif

	UART_DMA_TX_SendBlock(UART_DEBUG, string, len);

	#ifdef DEBUG_USE_RTOS
	xSemaphoreGive( dbg_semHandler );
	#endif
}

void user_debug_native_byte(unsigned char c)
{
	#ifdef DEBUG_USE_RTOS
	xSemaphoreTake( dbg_semHandler, ( TickType_t ) osWaitForever );
	#endif

	UART_DMA_TX_SendByte(UART_DEBUG, c);

	#ifdef DEBUG_USE_RTOS
	xSemaphoreGive( dbg_semHandler );
	#endif
}

