/*
    DEBUG ARDUINO

    2/3/2019
    Tran Vo Phuoc Dat
*/
#ifndef __DEBUG_H
#define __DEBUG_H

#include "main.h"
#include "uart/uart_services.h"


#ifdef __cplusplus
extern "C" {
#endif

    static void _data_print_uart(uint8_t *_data, uint32_t _size);
    void user_debug_init(void);
    void user_debug_print(int level, const char *module, int line, const char *fmt, ...);
    void user_debug_print_error(const char *module, int line, int ret);
    void user_debug_print_exact(const char *fmt, ...);
    void user_debug_native(char *buffer, int len);
    void user_debug_native_byte(unsigned char c);
#if __DEBUG__ > 0
#ifndef __MODULE__
#error "__MODULE__ must be defined"
#endif
#endif

#if __DEBUG__ >= 1
#define error(...)                                              \
    do                                                          \
    {                                                           \
        user_debug_print(1, __MODULE__, __LINE__, __VA_ARGS__); \
    } while (0)
#define ERROR(...)                                              \
    do                                                          \
    {                                                           \
        user_debug_print(1, __MODULE__, __LINE__, __VA_ARGS__); \
    } while (0)
#else
#define error(...)
#define ERROR(...)
#endif

#if __DEBUG__ >= 2
#define WARN(...)                                               \
    do                                                          \
    {                                                           \
        user_debug_print(2, __MODULE__, __LINE__, __VA_ARGS__); \
    } while (0)
#else
#define WARN(...)
#endif

#if __DEBUG__ >= 3
#define info(...)                                               \
    do                                                          \
    {                                                           \
        user_debug_print(3, __MODULE__, __LINE__, __VA_ARGS__); \
    } while (0)
#define INFO(...)                                               \
    do                                                          \
    {                                                           \
        user_debug_print(3, __MODULE__, __LINE__, __VA_ARGS__); \
    } while (0)
#define LOG(...)                                                \
    do                                                          \
    {                                                           \
        user_debug_print(3, __MODULE__, __LINE__, __VA_ARGS__); \
    } while (0)
#else
#define info(...)
#define INFO(...)
#define LOG(...)
#endif

#if __DEBUG__ >= 4
#define DEBUG(...)                                              \
    do                                                          \
    {                                                           \
        user_debug_print(4, __MODULE__, __LINE__, __VA_ARGS__); \
    } while (0)
#define DEBUGX(...)                          \
    do                                       \
    {                                        \
        user_debug_print_exact(__VA_ARGS__); \
    } while (0)
#define DEBUG_NATIVE(str, len) user_debug_native(str, len)
#define debug(...)                                              \
    do                                                          \
    {                                                           \
        user_debug_print(4, __MODULE__, __LINE__, __VA_ARGS__); \
    } while (0)
#define debugx(...)                          \
    do                                       \
    {                                        \
        user_debug_print_exact(__VA_ARGS__); \
    } while (0)
#define debug_native(str, len) user_debug_native(str, len)
#else
#define DEBUG(...)
#define DEBUGX(...)
#define DEBUG_NATIVE(str, len)

#define debug(...)
#define debugx(...)
#define debug_native(str, len)
#endif

#ifdef __cplusplus
}
#endif
#endif // __DEBUG_H