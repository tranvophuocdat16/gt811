#include "debug_uart_conf.h"
__IO uint32_t micro;

uint32_t micros(void)
{
	micro = HAL_GetTick();
	return micro;
}