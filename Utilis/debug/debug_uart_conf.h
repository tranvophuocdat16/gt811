/*
    app conf



    Tran Vo Phuoc Dat
    date: 08/03/2019
*/
#ifndef __DEBUG_AURT_CONF_H
#define __DEBUG_AURT_CONF_H

#include "main.h"
#include "uart/uart_services.h"

#ifdef __cplusplus
extern "C" {
#endif

#define UART_DEBUG (&huart1_dma)

// Export debug and console function!
#define UART_DEBUG_SendByte(c) UART_DMA_TX_SendByte(UART_DEBUG, c)
#define UART_DEBUG_SendBlock(block, len) UART_DMA_TX_SendBlock(UART_DEBUG, block, len)
#define UART_CONSOLE_Available()	UART_DMA_RX_Available(UART_DEBUG)
#define UART_CONSOLE_Flush() UART_DMA_RX_Flush(UART_DEBUG)
#define UART_CONSOLE_GetByte(c) UART_DMA_RX_Get(UART_DEBUG, c)

extern __IO uint32_t micro;

#ifdef __cplusplus
}
#endif

#endif //__DEBUG_AURT_CONF_H